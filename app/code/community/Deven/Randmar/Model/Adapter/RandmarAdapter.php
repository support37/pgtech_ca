<?php

namespace Randmar {

    class RandmarAdapter {

        const RANDMAR_URL_BASE = 'https://api.randmar.com/';

        private $userName;
        private $password;
        private $debug;
        private $defaultPictureUrl = "https://pgtech.ca/logo_pgtech_no_data.jpg";
        private $defaultIconPath = "/media/localisateur/icones/";

        function __construct($userName, $password, $debug = false) {
            $this->userName = $userName;
            $this->password = $password;
            $this->debug = $debug;
        }

        function getManufacturers() {

            $manufacturersJSON = '[{"Id":1,"Name":"Brother"}, {"Id":8,"Name":"Canon"}, {"Id":2,"Name":"Epson"}, {"Id":3,"Name":"Hewlett-Packard"}, {"Id":4,"Name":"Lexmark"}, {"Id":5,"Name":"Okidata"}, {"Id":6,"Name":"Samsung"}, {"Id":7,"Name":"Xerox"}]';
            $manufacturers = json_decode($manufacturersJSON, false);

            return $this->adjustObject($manufacturers);
        }

        function getPrintersByManufacturerId($manufacturerId) {
            $manufacturer = $this->retriveDataFromResellerCartridgeFinderAPI('manufacturers', $manufacturerId);
            return $manufacturer;
        }

        function retrievePrinterByTitle($printerTitle) {
            $printer = $this->retriveDataFromResellerCartridgeFinderAPI('printer', $printerTitle);
            return $printer;
        }

        function retrieveSupplyByMPN($supplyMPN) {
            $supply = $this->retriveDataFromResellerCartridgeFinderAPI('supply/MPN', $supplyMPN);
            return $supply;
        }

        function searchByName($nameSearched) {
            $products = $this->retriveDataFromResellerCartridgeFinderAPI('globalsearch', $nameSearched);
            return $products;
        }

        function getPriceProfiles() {

            $priceProfilesJSON = '["BRO","BRP","CAN","CNP","EPS","HP1","HP2","HP3","HP4","HP5","HPC","HPR","HPP","LEI","LEM","LEX","LXP","LXX","OKI","OKP","SAM","SMP","XER","XNS"]';
            $priceProfiles = json_decode($priceProfilesJSON, false);

            return $this->adjustObject($priceProfiles);
        }

        private function retriveDataFromResellerCartridgeFinderAPI($endpoint, $id = '') {

            $url = self::RANDMAR_URL_BASE . 'v1/resellercartridgefinder/' . $endpoint;

            if ($id != '') {
                $url .= '/' . $id;
            }

            $url .= '?username=' . $this->userName . '&password=' . $this->password;

            $info = $this->retriveDataFromURL($url);
            $info = $this->adjustObject($info);

            if (isset($info) && isset($info->StatusCode) && $info->StatusCode == "200")
            {
                return $info->Content;
            }

            return null;
        }

        private function retriveDataFromURL($url)
        {
            $url = str_replace (' ', '%20', $url);
            $url = strtolower($url);

            if ($this->debug)
            {
                echo $url . '<br />';
            }

            // http://stackoverflow.com/questions/16700960/how-to-use-curl-to-get-json-data-and-decode-the-data/16701318#16701318
            // Initiate curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Disable SSL verification
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Will return the response, if false it print the response
            curl_setopt($ch, CURLOPT_URL, $url); // Set the url
            $result=curl_exec($ch);
            curl_close($ch);

            return json_decode($result, false);
        }

        private function adjustObject($oldObject)
        {
            try {

                $object = $oldObject;
                foreach ($object as $key => $value) {

                    if (is_array ($value))
                    {
                        $object->$key = $this->adjustObject($value);
                    }
                    else if (is_object($value))
                    {
                        $adjustedObject = $this->adjustObject($value);
                        if(isset($object->$key)) {
                            $object->$key = $adjustedObject;
                        } else {
                            $object[$key] = $adjustedObject;
                        }
                    }
                    else if($key == "Picture" && is_null($value))
                    {
                        $object->$key = $this->defaultPictureUrl;
                    }
                }

                if (isset($object->ProductType) && $object->ProductType == 'Printer'){
                    $object->TitleFr = "Do not use";
                    $object->TitleEn = "Do not use";
                    $object->DescriptionFr = "Do not use";
                    $object->DescriptionEn = "Do not use";

                    $object->Usb = "Do not use";
                    $object->PaperLetter = "Do not use";
                    $object->PaperLegal = "Do not use";
                    $object->PaperExecutive = "Do not use";
                    $object->PaperA4 = "Do not use";

                    $object->IconTechnology = $object->Technology == null ? null : $this->defaultIconPath . strtolower ($object->Technology) . '.png';
                    $object->IconColor = $object->Color == null ? null : $this->defaultIconPath . strtolower ($object->Color) . '.png';
                    $object->IconCopy = $object->Copy ? $this->defaultIconPath . 'copy.png' : null;
                    $object->IconScan = $object->Scan ? $this->defaultIconPath . 'scan.png' : null;
                    $object->IconFax = $object->Fax ? $this->defaultIconPath . 'fax.png' : null;
                    $object->IconDuplex = $object->Duplex ? $this->defaultIconPath . 'duplex.png' : null;
                    $object->IconNetworkWireless = $object->NetworkWireless ? $this->defaultIconPath . 'wireless.png' : null;
                    $object->IconNetworkWired = $object->NetworkWired ? $this->defaultIconPath . 'network.png' : null;

                } else if (isset($object->ProductType) && $object->ProductType == 'Supply'){
                    $object->Icon = $this->defaultIconPath . strtolower ($object->Technology . $object->Color) . '.png';
                }

                return $object;

            } catch (Exception $e) {
            }

            return $oldObject;
        }
    }
}

?>
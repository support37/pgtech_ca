<?php

/**
 * Created by PhpStorm.
 * User: Welcome
 * Date: 1/18/2018
 * Time: 3:14 PM
 */
class Deven_Randmar_Block_Randmar_Printer extends Deven_Randmar_Block_Randmar
{
    public function getManufacturerPrinter()
    {
        return $this->getRandmarAdapter()->retrievePrinterByTitle(str_replace(' ', '-',$this->getData('printerTitle')));
    }
}
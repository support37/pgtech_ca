<?php

/**
 * Created by PhpStorm.
 * User: NN
 * Date: 1/19/2018
 * Time: 11:12 PM
 */
class Deven_Randmar_Block_Randmar_Search extends Deven_Randmar_Block_Randmar
{

    public function getSearchFormHtml() {

        //echo "<H1>" . "RANDMAR SEARCH PAGE " . $to_s . "</H1>";
        //print_r(get_locale());
        $_REQUEST['l'] = 'randmar';
        $_REQUEST['a'] = 'srch';//gpsfn';
        $_REQUEST['id'] = 451;

        $html .= "\t<div id='randmar-locator' siteurl='".$this->getBaseUrl()."'>\n";
        $html .= "<div class='overlay'><div class='loading'><img src='".$this->getSkinUrl('/assets/imgs/loading-2.gif')."'></div></div>";
        $html .= "\t<input id='search-input' class='search-input same-row-left' value='' placeholder='".Mage::helper('deven_randmar')->__('Tapez un num&eacute;ro de cartouche')."' />\n";
        //$html .= "\t<div id='search-button' class='black-button same-row-left search-button-randmar'>".Mage::helper('deven_randmar')->__('LOCALISER')."</div>\n";
        $html .= "\t<a id='search-a' class='black-button same-row-left search-button-randmar' href='#' onclick='searchRedirect();'>".Mage::helper('deven_randmar')->__('LOCALISER')."</a>\n";
        $html .= "\t<div class='end-row'></div>\n";
        $html .= "\t<div class='search-results-container'>\n";
        $html .= "\t</div><!-- search-results-container -->\n";
        $html .= "\t</div><br /><br /><!-- randmar-locator-->";

        $script = "
	<script type=\"text/javascript\">
//	$ = jQuery;
		jQuery('#search-button').click(function() {
			u = jQuery('#randmar-locator').attr('siteurl') + '/randmar/ajax/index';
			u += '?l=randmar';
			u += '&a=srch';
			u += '&cn='+jQuery('.search-input').val();
			//u += '&id='+$('.printer-select').val();
			showLoading();
			jQuery.ajax({
				url: u,
				method: \"POST\",
				success: function(result) {
				console.log(result);
					/*if(jQuery('.cartridge-result').length>0){
						jQuery('.cartridge-result').remove();
					}
					if(jQuery('.no-results').length>0){
						jQuery('.no-results').remove();
					}
					if(jQuery('.cartridge-result-rule').length>0){
						jQuery('.cartridge-result-rule').remove();
					}

					jQuery('.search-results-container').html(result);
					if(jQuery('.from-backend').length>0) {
						jQuery('.from-backend').hide();
					}
					hideLoading();
					addClickListenerToAddButton();
					addEnterListenerToQtyInput();*/
				},
				error: function(result) {
					hideLoading();
				}
			});
		});
		
		function searchRedirect(){
	new_url = jQuery('#randmar-locator').attr('siteurl') + '/randmar/index/search/' + jQuery('#search-input').val() + '/';
	//alert(new_url);
	event.preventDefault();
	jQuery('#search-a').attr('href',new_url);
	window.location = new_url;
}
	</script>";
        return $html . $this->getStyles() . $script;
    }
}